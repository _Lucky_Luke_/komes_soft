#define F_CPU 16000000
#include <util/delay.h>
#include <avr/io.h>

#define true 1
#define false 0

uint8_t counter;
uint8_t lighttime[8]; // czas swiecenia.
uint8_t risingTable[8];
uint32_t totalTime;
uint8_t currentBrightestLed;

void setup()
{	
	DDRA = 0xFF;
	DDRB = 0x00;
	PORTA = 0x00;
	PORTB = 0x01;
	totalTime = 0;
	currentBrightestLed = 0;
	for (uint8_t x = 0; x < 8; ++x)
	{
		lighttime[x] = 21 - 3 * x;
		risingTable[x] = true;
	 }
}

void ReliableLight()
{
	for(uint8_t x = 0; x < 25; x++)
	{
		for(uint8_t y = 0; y<8; y++)
		{
			if(lighttime[y] >= x)
			{
				PORTA = PORTA | (1<<y);
			}
			else
			{
				PORTA = !((!PORTA) | (1<<y));
			}
		}
		_delay_ms(1);
	}
}	

void ProfessionalWave()
{
	ReliableLight();
	
	for (int x = 0; x < 8; ++x)
	{
		if (risingTable[x])
		{
			if (lighttime[x] == 25)
			{
				risingTable[x] = false;
				--lighttime[x];
			}
			else ++lighttime[x];
		}
		else
		{
			if (lighttime[x] == 0)
			{
				risingTable[x] = true;
				++lighttime[x];
			}
			else --lighttime[x];
		}
	}
}

void ProfessionalPingPong()
{	
	if (totalTime == 0 || totalTime > 73000)
	{
		for (int x = 0; x<6; x++) lighttime[x] = 25 - 5 * x;
		lighttime[6] = lighttime[7] = 0;
		totalTime = 0;
	}
	ReliableLight();
		
	if (totalTime%1000 == 0 && totalTime >= 1000 && totalTime <= 35000)
	{
		for (int i = 0; i <= currentBrightestLed; i++)
		{
			if (lighttime[i] > 0)
				lighttime[i] -= 1;
		}
		for (int i = currentBrightestLed+1; i < 8; i++)
		{
			if (lighttime[i-1]>5)
				lighttime[i] += 1;
		}
		if (currentBrightestLed < 7 && lighttime[currentBrightestLed + 1] == 25)
		{
			currentBrightestLed++;
		}
	}
	if (totalTime%1000 == 0 && totalTime > 35000 && totalTime <= 73000)
	{
		for (int i = currentBrightestLed; i < 8; i++)
		{
			if (lighttime[i] > 0)
				lighttime[i] -= 1;
		}
		for (int i = 0; i < currentBrightestLed; i++)
		{
			if (lighttime[i + 1]>5)
				lighttime[i] += 1;
		}
		if (currentBrightestLed >0 && lighttime[currentBrightestLed - 1] == 25)
		{
			currentBrightestLed--;
		}
	}
	totalTime += 25;
}

void AmateurLedFunction()
{
	counter = 1;
	while(counter <= 0x80 && counter != 0)
	{
		PORTA = counter;
		counter = counter << 1;
		_delay_ms(100);
	}
	counter = 0x40;
	while(counter > 0x01 && counter <= 0x40)
	{
		PORTA = counter;
		counter = counter >> 1;
		_delay_ms(100);
	}		
}

void ButtonActivator()
{
	if(!(PINB & 0x01))
	{
		AmateurLedFunction();		
	}	
}

int main(void)
{	
	setup();	
    while(1)
    {
		ProfessionalPingPong();
		//ProfessionalWave();
		//ButtonActivator();
		//AmateurLedFunction();
    }
}