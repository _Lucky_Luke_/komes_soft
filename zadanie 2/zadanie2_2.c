#define F_CPU 16000000
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

void setup()
{
	OCR0 = 0;
	DDRA = 0xFF;
	DDRC = 0x00;
	PORTA = 0x00;
	DDRB = 0xFF;
	PORTC = 0x03;
	TCCR0 = (1 << WGM01) | (1 << WGM00) | (1 << CS01) | (1 << COM01);
	
	sei();
}

int main(void)
{
	setup();
	while(1)
	{
		if (!(PINC & 0x01)) OCR0++;
		if (!(PINC & 0x02)) OCR0--;
		_delay_ms(10);
	}
}