#define F_CPU 16000000
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t counter;

ISR(INT0_vect)
{
	++counter;
	uint8_t tmp = counter << 1;
	tmp |= (PORTA & 0x01);
	PORTA = tmp;
}

void setup()
{
	counter = 0;
	DDRA = 0xFF;
	DDRD = 0x00;
	PORTA = 0x00;
	PORTD = 0x04;
	GICR = 1<<INT0;
	MCUCR = (1<<ISC01) | (1<<ISC00);
	sei();
}

int main(void)
{
	setup();
	while(1)
	{
		PORTA ^= 0x01;
		_delay_ms(1000);
	}
}