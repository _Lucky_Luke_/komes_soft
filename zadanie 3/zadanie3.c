#define F_CPU 16000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h>

uint16_t counter;
uint16_t millisCounter;
uint8_t digit;
uint8_t digits[10];

ISR(TIMER0_COMP_vect)
{
    PORTA = digits[(counter / ((int16_t)pow(10.0, digit))) % 10];
	PORTB = ~(1 << digit);
    millisCounter++;
    if (millisCounter == 1000)
    {
        millisCounter = 0;
        counter++;
    }
    digit = (digit + 1) % 4;
}

void setup()
{
    DDRA = 0xFF;
    DDRB = 0x0F;
    PORTA = 0xFF;
    PORTB = 0;
    digits[0] =~ 0b00111111;
    digits[1] =~ 0b00000110;
    digits[2] =~ 0b01011011;
    digits[3] =~ 0b01001111;
    digits[4] =~ 0b01100110;
    digits[5] =~ 0b01101101;
    digits[6] =~ 0b01111101;
    digits[7] =~ 0b01000111;
    digits[8] =~ 0b01111111;
    digits[9] =~ 0b01101111;
    counter = 0;
    millisCounter = 0;
    digit = 0;
    OCR0 = 250;
    TCCR0 = (1 << WGM01) | (1 << CS01) | (1 << CS00);
    TIMSK |= (1 << OCIE0);
    sei();
}

int main(void)
{
    setup();
    while(1)
    {
    }
}