#include "lcd.h"

void WriteNibble(unsigned char nibbleToWrite)
{
	PORTA |= 0x08;
	PORTA = (PORTA & 0x0F) | ((0x0F & nibbleToWrite) << 4);
	PORTA ^= 0x08;
}

void WriteByte(unsigned char dataToWrite)
{
	WriteNibble(dataToWrite >> 4);
	WriteNibble(dataToWrite);
}

void LCD_Command(unsigned char _i)
{
	PORTA &= ~0x04;
	WriteByte(_i);
	_delay_ms(2);
};

void LCD_Text(char * _t)
{
	PORTA |= 0x04;
	for (uint8_t x = 0; x < strlen(_t); ++x)
	{
		WriteByte(_t[x]);
		_delay_ms(5);
	}	
};

void LCD_GoToXY(unsigned char _x, unsigned char _y)
{
	LCD_Home();
	_delay_ms(2);
	for (uint8_t x = 0; x < _y * 40 + _x; ++x) {LCD_Command(0x14);_delay_ms(5);}
};

void LCD_Clear(void)
{
	LCD_Command(0x01);
};

void LCD_Home(void)
{
	LCD_Command(0x02);
};

void LCD_Initalize(void)
{
	for(uint8_t i =0; i<3; i++)
	{
		_delay_ms(100);
		WriteNibble(0x03);
	}
	_delay_ms(5);
	WriteNibble(0x02);
	_delay_ms(5);
	LCD_Command(0x28);
	_delay_ms(5);
	LCD_Command(0x08);
	_delay_ms(5);
	LCD_Command(0x01);
	_delay_ms(5);
	LCD_Command(0x06);
	_delay_ms(5);
	LCD_Command(0x0f);
	LCD_Command(0x80);
};

